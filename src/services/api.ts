import axios, {AxiosInstance} from "axios";

axios.defaults.baseURL = "https://jsonplaceholder.typicode.com";

const instance: AxiosInstance = axios.create({});

export default instance;
