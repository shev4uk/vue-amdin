import dashboardRoutes from "@/modules/dashboard/router";
import usersRoutes from "@/modules/users/router";

export const routes = [
	dashboardRoutes,
	usersRoutes,
];
