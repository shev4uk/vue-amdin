import {createRouter, createWebHistory} from "vue-router";
import type {Router} from "vue-router";
import { routes } from "./routes.ts";

export const router: Router = createRouter({
	history: createWebHistory(),
	routes,
	// scrollBehavior(to, from, savedPosition) {
	// 	if (savedPosition) {
	// 		return savedPosition
	// 	} else {
	// 		return { top: 0 }
	// 	}
	// },
});
