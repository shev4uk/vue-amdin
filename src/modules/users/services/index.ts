import api from "@/services/api.ts";

export interface IUser {
	"id": number;
	"name": string;
	"username": string;
	"email": string;
	"address": {
		"street": string;
		"suite": string;
		"city": string;
		"zipcode": string;
		"geo": {
			"lat": string;
			"lng": string;
		}
	},
	"phone": string;
	"website": string;
	"company": {
		"name": string;
		"catchPhrase": string;
		"bs": string;
	}
}

interface AllUsersPromise {
	data: IUser[];
}

export const getAllUsers = (): Promise<AllUsersPromise> => api.get("https://jsonplaceholder.typicode.com/users");
