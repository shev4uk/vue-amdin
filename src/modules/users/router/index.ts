const usersRoutes = {
	path: "/users",
	name: "usersPage",
	component: () => import(/* webpackChunkName: projects */ "../views/usersPage.vue"),
	meta: {
		layout: "default",
	},
};

export default usersRoutes;
