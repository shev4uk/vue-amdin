import {ref} from "vue";
import {getAllUsers, IUser } from "@/modules/users/services";

export const useUsers = () => {
	const users = ref<IUser[]>([]);
	const loading = ref(false);

	const fetchUsers = async () => {
		loading.value = true;

		try {
			const { data } = await getAllUsers();
			users.value = data;
		} catch (e) {
			console.error(e);
		} finally {
			loading.value = false;
		}
	};

	const getUserById = (id: number) => users.value?.find((item: IUser) => item.id === id);

	return {
		users,
		loading,
		fetchUsers,
		getUserById,
	};
};
