const dashboardRoutes = {
	path: "/",
	name: "DashboardPage",
	component: () => import(/* webpackChunkName: personal */ "../views/DashboardPage.vue"),
	meta: {
		layout: "default",
	},
};

export default dashboardRoutes;
